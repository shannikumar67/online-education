Website for online education

This is an online learning platform where students can study a range of subjects and skills at their convenience. A wide variety of courses given by qualified professors are available on our website. The time and place of access to course materials and resources is completely up to the student.

Features
A course catalogue offering a range of subjects and abilities
Course materials like lectures, videos, and homework
Personalised learning experience for students and teachers through online forums and debates
Progress monitoring and certificates of completion of the course
Mobile-friendly design for on-the-go learning

Utilised front-end development technologies

JScript, CSS, and HTML 

Many thanks